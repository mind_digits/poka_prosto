# coding=utf-8
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from threading import * #Timer, Thread
import telegram
import sqlite3
import logging
import datetime
from time import sleep
import random
import copy
# import httplib2
# import apiclient.discovery
# from oauth2client.service_account import ServiceAccountCredentials


class Game:
    def __init__(self, id, title, group):
        self.id = id
        self.title = title
        self.people = [[]]
        self.date = str
        self.dat = datetime.datetime
        self.time = str
        self.place = str
        self.info = str
        self.max = int
        self.res = 6
        self.timer = None
        self.group = group

class User:
    def __init__(self, id):
        self.id = id
        self.name = "new"
        self.lastclick_spisok = datetime.datetime(2019, 1, 1)
        self.lastclick_join = datetime.datetime(2019, 1, 1)
        self.state = 1 #1 - ждем имя, 11 - имена друзей, 20 - новая игра
        self.data = ''
        self.prev_mess_text = None
        self.prev_mess_id = ''
        self.chatid = ''

    def save_keyboard(self, messid, text):
        self.prev_mess_id = messid
        self.prev_mess_text = text

    def destroy_keyboard(self, bot, update):
        empty_markup = telegram.InlineKeyboardMarkup([[]])
        if self.prev_mess_text is not None:
            bot.edit_message_text(chat_id=update.message.chat_id,
                                  text=self.prev_mess_text,
                                  message_id=self.prev_mess_id,
                                  reply_markup=empty_markup, parse_mode=telegram.ParseMode.MARKDOWN)
            self.prev_mess_text = None
            self.prev_mess_id = ''


class PokaTimer():
    def __init__(self, t, hFunction, bot, g):
        self.t = t
        self.hFunction = hFunction
        self.thread = Timer(self.t, self.hFunction, [bot, g])

    def start(self):
        self.thread.start()

    def cancel(self):
        self.thread.cancel()


class Users:
    def __init__(self):
        self.all = []

class Games:
    def __init__(self):
        self.all = []
        self.timers = []
        self.links = []
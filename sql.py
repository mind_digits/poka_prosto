# coding=utf-8
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from threading import * #Timer, Thread
import telegram
import sqlite3
import logging
import datetime
from time import sleep
import random
import copy
# import httplib2
# import apiclient.discovery
# from oauth2client.service_account import ServiceAccountCredentials
from sql import *
from classes import *
from settings import *

def addusertodb(id, name):
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    sql = "SELECT * FROM users WHERE id = ?"
    cursor.execute(sql, (id,))
    a = cursor.fetchall()
    if len(a) == 0:
        print('Новенький, добавляю', name)
        cursor.execute("""INSERT INTO users VALUES (?,?)""", (id, name))
        conn.commit()
    else:
        print('Есть в базе ', a[0][0])
        if (str(name) == 'new') and (str(a[0][1]) == 'stranger'):
            print('Незнакомец получил пароль')
            cursor.execute("""UPDATE users SET name = (?) WHERE id = (?)""", (name, id))
            conn.commit()
        if str(a[0][1]) == 'new':
            print('Пользователь ввел имя', name)
            cursor.execute("""UPDATE users SET name = (?) WHERE id = (?)""", (name, id))
            conn.commit()


def get_games_info(bot):
    games.all = []

    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    sql = "SELECT * FROM games"
    cursor.execute(sql)
    a = cursor.fetchall()
    for i in a:
        #формат строки с игрой
        #i[0] id text, i[1] groupid, i[2] date text, i[3] time text, i[4] place text, i[5] max text, i[6] info text
        #сперва парсим дату, если еще не наступила, запоминаем
        now = datetime.datetime.now()
        gdat = datetime.datetime(int(i[2].split('/')[2]), int(i[2].split('/')[1]), int(i[2].split('/')[0]),
                                  int(i[3].split(':')[0]), int(i[3].split(':')[1]), 0, 0)
        delta = now - gdat
        if delta.days < 0:
            if len(games.all):
                games.all.append(copy.copy(games.all[-1]))
            else:
                games.all = []
                games.all.append(Game(1234567890, "01/01", 1000001))
            g = games.all[-1]
            g.id = i[0]
            g.group = i[1]
            g.dat = gdat
            g.date = i[2]
            g.time = i[3]
            g.place = i[4]
            g.info = i[6]
            g.max = i[5]
            g.people = []
            sql = "SELECT name FROM players where gameid = (?)"
            cursor.execute(sql, [g.id])
            for j in cursor.fetchall():
                g.people.append(j)

def check_players_amount(one_game):
    max = int(one_game.max)
    res = int(one_game.res)
    current = int(len(one_game.people))
    if current < max:
        return max - current #количество всех свободных мест
    elif current < (max + res):
        return current - (max + res) #количество резервных
    elif current >= (max + res):
        return 0 #мест нет :(

def archivegame(argame):
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()

    '''firstgame = games.all[-1]
    for g in games.all:
        # print('game date %s - var %s = %s' %(g.dat,lastdate,(g.dat - lastdate)))
        delta = g.dat - firstgame.dat
        # print('cur %s, last %s, delta %s'%(g.dat.day,lastdate.day,delta.days))
        if delta.days < 0:
            firstgame = g'''

    for p in argame.people:
        print(argame.people, p)
        cursor.execute("""INSERT INTO players (gameid, name) VALUES (?,?)""", (argame.id, p[0]))
    cursor.execute("""INSERT INTO games (id, date, time, place, max, info) VALUES (?,?,?,?,?,?)""",
                   (argame.id, argame.date, argame.time, argame.place, argame.max, argame.info))
    conn.commit()
    batch_update_spreadsheet_request_body = {
        'requests': [{
            'deleteSheet': {
                'sheetId': argame.id
            }
        }]
    }
    req = service.spreadsheets().batchUpdate(spreadsheetId=spr_id, body=batch_update_spreadsheet_request_body)
    req.execute()


def gametodb(dbgame):
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    for p in dbgame.people:
        #print(dbgame.people, p)
        cursor.execute("""INSERT INTO players (gameid, name) VALUES (?,?)""", (dbgame.id, p[0]))
    cursor.execute("""INSERT INTO games (id, date, time, place, max, info) VALUES (?,?,?,?,?,?)""",
                   (dbgame.id, dbgame.date, dbgame.time, dbgame.place, dbgame.max, dbgame.info))
    conn.commit()

def update_game_data(gameid, ifdate=False, date='', iftime=False, time='', ifplace=False, place='', ifinfo=False,
                     info='', ifmax=False, nmax=0, cleanusers=False):
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    if cleanusers:
        cursor.execute("""DELETE FROM players WHERE gameid = (?)""", (gameid,))
    if ifdate:
        cursor.execute("""UPDATE games SET date = (?) WHERE id = (?)""", (date, gameid))
    if iftime:
        cursor.execute("""UPDATE games SET time = (?) WHERE id = (?)""", (time, gameid))
    if ifplace:
        cursor.execute("""UPDATE games SET place = (?) WHERE id = (?)""", (place, gameid))
    if ifmax:
        cursor.execute("""UPDATE games SET max = (?) WHERE id = (?)""", (nmax, gameid))
    if ifinfo:
        cursor.execute("""UPDATE games SET info = (?) WHERE id = (?)""", (info, gameid))
    conn.commit()

def join_game_db(gameid,name):
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    cursor.execute("""INSERT INTO players (gameid, name) VALUES (?,?)""", (gameid, name))
    conn.commit()

def leave_game_db(gameid,name):
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    cursor.execute("""DELETE FROM players WHERE gameid = (?) AND name = (?)""", (gameid, name))
    conn.commit()

def delgamefromdb(dbgame):
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    cursor.execute("""DELETE FROM games WHERE id = (?)""", (dbgame,))
    cursor.execute("""DELETE FROM players WHERE gameid = (?)""", (dbgame,))
    conn.commit()


def get_all_users(): #подтягивает всех юзеров во внутренний массив

    users.all = []  # обнуляем всех юзеров

    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    sql = "SELECT * FROM users"
    cursor.execute(sql)
    a = cursor.fetchall()
    for i in a:
        if str(i[1]) != 'stranger':
            users.all.append(User(i[0]))
            users.all[-1].name = i[1]
    print('NUMBER OF USERS %s' % len(a))
    for u in users.all: print('Name %s' % u.name)

def update_users_db(): #проверяем, синхронизирована ли бд и программный массив
                        #если в базе больше людей - создаем под них новых юзеров
                        #
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    sql = "SELECT * FROM users"
    cursor.execute(sql)
    a = cursor.fetchall()

    for i in a:
        ifbraked = False
        for u in users.all:
            if str(i[1]) == str(u.name):
                u.name = str(i[1])
                ifbraked = True;
                break
        if (not ifbraked) and (str(i[1]) != 'stranger'):
            # если не нашли пересечений в массивах, то добавляем нового юзера в программный массив
            # игнорируем незнакомцев
            users.all.append(User(str(i[0])))
            users.all[-1].name = str(i[1])
            print('Len of userlist', len(users.all))
            print('Found new user %s' % users.all[-1].name)

def invite(bot, update):
    if (str(update.message.from_user.id) in admin_list):
        key = random.randint(100000, 999999)
        conn = sqlite3.connect("mydatabase.db")
        cursor = conn.cursor()
        cursor.execute("""INSERT INTO keys VALUES (?)""", [100001, str(key), 0])
        conn.commit()
        bot.send_message(chat_id=update.message.chat_id,
                         text='https://t.me/poka_prosto_voleybot?start=%d' % key)

def insert(bot, update, args):
    if (str(update.message.from_user.id) in admin_list):
        newname = ''
        for k in args[1:]:
            newname = newname + str(k) + ' '
        newname = newname[:-1]
        conn = sqlite3.connect("mydatabase.db")
        cursor = conn.cursor()
        sql = "SELECT * FROM users WHERE id = (?)"
        cursor.execute(sql, [args[0]])
        if len(cursor.fetchall()) > 0:
            cursor.execute("""UPDATE users SET name = (?) WHERE id = (?)""", (newname, args[0]))
            conn.commit()
            bot.send_message(chat_id=update.message.chat_id,
                             text='Пользователь с айди %s переименован в %s' % (args[0], newname))
        else:
            cursor.execute("""INSERT INTO users (id, name) VALUES (?,?)""", [args[0], newname])
            conn.commit()
            bot.send_message(chat_id=update.message.chat_id,
                             text='Пользователь с айди %s добавлен под именем %s' % (args[0], newname))
        get_all_users()


def kick(bot, update, args):
    if (str(update.message.from_user.id) in admin_list):
        if len(args):
            conn = sqlite3.connect("mydatabase.db")
            cursor = conn.cursor()
            sql = "DELETE FROM users WHERE id = (?)"
            cursor.execute(sql, [args[0]])
            conn.commit()
            print('Кикнул юзера', args[0])


def showusers(bot, update):
    if (str(update.message.from_user.id) in admin_list):
        conn = sqlite3.connect("mydatabase.db")
        cursor = conn.cursor()
        sql = "SELECT * FROM users"
        cursor.execute(sql)
        usertext = 'Пользователи\n'
        a = cursor.fetchall()
        print(len(a))

        for k in a:
            print(str(k[0]))
            usertext = usertext + str(k[0]) + ' ' + str(k[1]) + '\n'
        usertext = usertext + "\nВсего: %s" % len(a)
        bot.send_message(chat_id=update.message.chat_id, text=usertext)


def check_key(key):
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    sql = "SELECT * FROM keys"
    cursor.execute(sql)
    for k in cursor.fetchall():
        if str(k[1]) == str(key):
            if str(k[1]) == 0: #если группа приватная
                sql = "DELETE from keys WHERE key = (?)"
                cursor.execute(sql, [key])
            conn.commit()
            return 1
    return 0

def get_unique_id():
    #todo убрать рандом
    conn = sqlite3.connect("mydatabase.db")
    cursor = conn.cursor()
    newid = random.randint(100000, 999999)
    sql = "SELECT * FROM games WHERE id = (?)"
    cursor.execute(sql, [newid])
    if len(cursor.fetchall()) > 0:
        newid = random.randint(100000, 999999)
        sql = "SELECT * FROM games WHERE id = (?)"
        cursor.execute(sql, [newid])
    print('Generated new ID', newid)
    return newid


# coding=utf-8
from telegram.ext import Updater, CommandHandler, CallbackQueryHandler, MessageHandler, Filters
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from threading import * #Timer, Thread
import telegram
import sqlite3
import logging
import datetime
from time import sleep
import random
import copy
# import httplib2
# import apiclient.discovery
# from oauth2client.service_account import ServiceAccountCredentials
from sql import *
#from classes import *
from settings import *
import sys



def login(userid, chatid, bot):
    # проверяем, знаем ли юзера
    update_users_db()
    log = find_user(userid)
    # если не знаем
    if (log == 0) : #если не нашли по такому id
        if find_user_give_user(userid) is None:
            print('Who is it?')
            addusertodb(userid, 'stranger')
            bot.send_message(chat_id=chatid,
                             text="Привет!\nУ нас тут частная вечеринка. Чтобы продолжить общение со мной, тебе нужен "
                                  "инвайт от администратора.\n*¯\_(ツ)_/¯*",
                             parse_mode=telegram.ParseMode.MARKDOWN)
        return 0
    elif log == -1: bot.send_message(chat_id=chatid,
                         text="Упс, почему-то база юзеров пуста, возможно, упала свять с гугл таблицой, подожди 5 секунд и попробуй еще раз")
    else:
        print('Юзер залогинен', log)
        #addusertodb(userid, log)
        return 1  # если залогинен

def settimers(bot):
    now = datetime.datetime.now()
    for t in games.timers: t.cancel()
    games.timers = []
    for g in games.all:
        tdelta = datetime.timedelta(days=1)
        alarmdate = datetime.datetime(g.dat.year, g.dat.month, g.dat.day, alarmtime['hours'], alarmtime['mins'])
        alarmdate = alarmdate - tdelta
        #print(alarmdate)
        delta = (alarmdate - now).total_seconds()
        if delta > 0:
            gtimer = PokaTimer(delta, sendnotification, bot, g)
            print('Set up Timer in %s seconds' % delta)
            gtimer.start()
            games.timers.append(gtimer)
        print('Threads', active_count())

'''добавляем с вопросиком юзера
добавляем с вопросиком друга
добавляем в settimer обработку события за 2 суток до игры
всем, кто сам или чьи друзья в списке с вопросом - выдаем сообщение
"уважаемый, подтвердите участие, у вас есть сутки" и две кнопки, буду/нет
отдельное сообщение по каждому, кого он добавил с вопросом
если подтверждает, вопросик удаляется
через сутки, во время массового напоминания, если кто-то еще остался в списке с вопросом, он снимается с игры'''


def sendnotification(bot, g):
    for p in range(len(g.people)):
        for i in range(len(users.all)):
            u = users.all[i]
            if u.name == g.people[p][0]:
                print('Sending message to', u.name)
                if p <= int(g.max):
                    bot.send_message(chat_id=u.id, text="Алярм, *%s*!\nНапоминаю, ты записан на игру!\n%s, %s, %s,\n%s"
                                                         % (u.name, g.date, g.place, g.time, g.info),
                                     parse_mode=telegram.ParseMode.MARKDOWN)
                else:
                    bot.send_message(chat_id=u.id, text="Доброе утро, *%s*!\nЗавтра будет игра, ты в резерве.\n"
                                                        "%s, %s, %s,\n%s\n🤞 Держу за тебя пальцы, очень часто кто-то отваливается в последний момент"
                                                        % (u.name, g.date, g.place, g.time, g.info),
                                     parse_mode=telegram.ParseMode.MARKDOWN)
                sleep(1)
                break
    bot.send_message(chat_id=groupid, text="Тут это, завтра игра, вроде, намечается.\nПаш, не волнуйся, я всем напомнил",
                     parse_mode=telegram.ParseMode.MARKDOWN)



def join(bot, update, args):
    pass



def start(bot, update, args):
    if str(update.message.chat_id) != str(groupid):
        if len(args) > 0:
            if check_key(args[0]):
                bot.send_message(chat_id=update.message.from_user.id,
                                 text="Поздравляю с успешной регистрацией!\nМы пока незнакомы, но сейчас это исправим.\n*Введи своё имя.*\nЖелательно сделать его "
                                      "узнаваемым для других игроков, вряд ли они знают тебя под никнеймом типа _cyberdroid1979_",
                                 parse_mode=telegram.ParseMode.MARKDOWN)

                addusertodb(update.message.from_user.id, 'new')
            else:
                bot.send_message(chat_id=update.message.from_user.id,
                                 text='Ключ не верифицирован')
        if login(update.message.from_user.id, update.message.chat_id, bot):
            if str(update.message.chat_id) != str(groupid):
                us = find_user_give_user(update.message.from_user.id)
                custom_keyboard = [['Список игр']]
                reply_markup = telegram.ReplyKeyboardMarkup(custom_keyboard, resize_keyboard=True)
                mes = bot.send_message(chat_id=update.message.chat_id, text="Привет, *%s*!\nЯ твой личный спортивный менеджер.\n"
                                                                      "Как насчет стать звездой волейбола?\nНет времени объяснять, срочно нажми Список игр."
                                 % us.name, reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
                settimers(bot)



def find_user(userid): #находит имя по айди
    #print('I\'m looking for ID %s' % userid)
    #print('Total users amount %s' % len(users.all))
    for u in users.all:
        if (str(u.id) == str(userid)) and (u.name != 'new'):
            #print('Found USER %s' % u.name)
            return u.name
    if len(users.all) == 0: return -1
    return 0

def find_user_give_user(userid): #ищет по айди, возвращает объект
    #print('I\'m looking for user object with ID %s' % userid)
    for u in users.all:
        if str(u.id) == str(userid):
            #print('Found USER object %s' % u.name)
            return u
    return None


def find_game(idx):
    g_found = None
    isfound = False
    for g in games.all:
        #print('Current id is %s' % g.id)
        if (str(g.id) == str(idx)):
            g_found = g
            isfound = True
            break
    if isfound:
        #print ('I found game: %s' % g_found.place)
        return g_found
    else:
        #print ('No game with id %s' % idx)
        return None

def amiingame(userid, gameid):
    #возвращает номер юзера и тех, кого он добавил, в указанной игре
    g = find_game(gameid)
    my_team = [-1, []]
    for i in range(0, len(g.people)):
        cur_guy = g.people[i][0].split(' <== ')
        if (len(cur_guy) == 1) and (g.people[i][0] == find_user(userid)):
            my_team[0] = i #добавили свой номер
            my_team[1].append(i)
        if (len(cur_guy) == 2) and (cur_guy[1] == find_user(userid)):
            my_team[1].append(i)  # добавили номер все, кого мы добавили
    #print(my_team)
    return my_team


def checktimeout(userid, sec, but):
    u = find_user_give_user(userid)
    now = datetime.datetime.now()
    if but == 1: #список игр
        then = u.lastclick_spisok
        delta = now - then
        #print(delta.seconds)
        if delta.seconds > sec:
            u.lastclick_spisok = now
            return 1
        else:
            #print('TOO FAST')
            return 0
    if but == 2:  # в игру
        then = u.lastclick_join
        delta = now - then
        print(delta.seconds)
        if delta.seconds > sec:
            u.lastclick_join = now
            return 1
        else:
            #print('TOO FAST')
            return 0

def get_date_with_zero(newdat):
    if newdat.day < 10: new_date = '0'
    else: new_date = ''
    new_date = new_date + str(newdat.day) + '/'
    if newdat.month < 10: new_date = new_date + '0'
    new_date = new_date + str(newdat.month) + '/' + str(newdat.year)
    return new_date

def notify_reserve(bot, i, g, c):
    if i <= (int(g.max) - 1):  # если удалился из основного состава
        if len(g.people) >= int(g.max):  # если кто-то был в резерве
            luckyname = g.people[int(g.max) - 1][0].split(' <== ')
            lucky_guy = None
            if len(luckyname) == 1:  # если чел добавлялся сам
                for u in users.all:
                    if str(luckyname[0]) == str(u.name):
                        lucky_guy = u
                        break

                if lucky_guy:  # если есть такой юзер
                    bot.send_message(chat_id=lucky_guy.id,
                                     text="🎈 Good news, *%s*! 🎈\nВ игре %s освободилось место.\nТы был в резерве, но "
                                          "теперь ты попал в основной состав.\nПохоже, сегодня твой день!"
                                          % (lucky_guy.name, g.date),
                                     parse_mode=telegram.ParseMode.MARKDOWN)
            elif len(luckyname) == 2:
                for u in users.all:
                    if str(luckyname[1]) == str(u.name):
                        lucky_guy = u
                        break
                if lucky_guy:  # если есть такой юзер
                    bot.send_message(chat_id=lucky_guy.id,
                                     text="Good news, *%s*!\nВ игре %s освободилось место.\nТвой друг *%s* был в резерве, но "
                                          "теперь попал в основной состав.\nПохоже, сегодня его (или её) день!"
                                          % (lucky_guy.name, g.date, luckyname[0]),
                                     parse_mode=telegram.ParseMode.MARKDOWN)
            bot.send_message(chat_id=groupid,
                             text="⬆️ Везунчик *%s* перемещается из резерва в основной состав *%s*.\nПохлопаем *%s*.\n"
                                  "Надеемся, что *%s* присоединится к нам в следующий раз!"
                                  % (luckyname[0], g.date, luckyname[0], c.split(' <== ')[0]),
                             parse_mode=telegram.ParseMode.MARKDOWN)
            if lucky_guy:
                print('Предупредил %s о переходе в основной состав' % lucky_guy.name)




def trista1(bot, update):
    print(update.message.chat_id)
    #bot.send_message(chat_id=update.message.chat_id, text="Отсоси у тракториста!")
    bot.send_message(chat_id=groupid, text="Паша запретил отвечать мне на эту команду :(")


def admin(bot, update): #зеркало в калбеке
    us = find_user_give_user(update.message.from_user.id)
    if (str(update.message.from_user.id) in admin_list):
        admin_keyboard = [[InlineKeyboardButton("Создать игру", callback_data='newgame')],
                          [InlineKeyboardButton("Изменить игру", callback_data='editsomegame')],
                          [InlineKeyboardButton("Удалить игру", callback_data='delsomegame')],
                          [InlineKeyboardButton("🔙", callback_data='gamelist')]]

        reply_markup = telegram.InlineKeyboardMarkup(admin_keyboard)
        us.destroy_keyboard(bot, update)
        mes = bot.send_message(chat_id=update.message.chat_id, text="Привет, избранный!\nЧем займемся?",
                               reply_markup=reply_markup)
        us.save_keyboard(mes['message_id'], mes['text'])


def react_inline(bot, update):

    query = update.callback_query
    us = find_user_give_user(query.from_user['id'])

    if ('gamelist' in query.data):
        us.state = 0
        games_keyboard = [[]]
        j = 0
        reply_markup = telegram.InlineKeyboardMarkup(games_keyboard)
        text_games = "Смотри, какие игры скоро состоятся:\nКуда запишемся, *%s*?" % find_user(query.from_user['id'])
        for g in games.all:
            emo_num = '%s️⃣ ' % str(j + 1)
            text_games = text_games + str("\n\n%s%s, %s, %s,\n%s" % (emo_num, g.date, g.place, g.time, g.info))
            num = check_players_amount(g)
            games_keyboard.append([])
            games_keyboard[j].append(
                InlineKeyboardButton("%sИгра %s" % (emo_num, g.date), callback_data='showgame:%s' % g.id))
            if (num == 0): text_games = text_games + str("\nМест в основном составе и резерве больше нет")
            if (num > 0): text_games = text_games + str("\nОсталось мест: %s" % (num))
            if (num < 0): text_games = text_games + str(
                "\nОсновной состав набран. Осталось мест в резерве: %s" % (-num))
            j = j + 1
        mes = bot.edit_message_text(text=text_games, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])


    if ('showgame' in query.data):
        one_game_keyboard = [[]]
        ichoosegame = query.data
        ichoosegame = ichoosegame.split(':')[1]
        print('Showing game %s to %s' % (ichoosegame, us.name))
        g = find_game(ichoosegame)
        text_games = 'Детали игры\n' + str("📅 Дата: %s\n🗺 Место: %s\n🕐 Начало: %s\nМест всего: %s\nИнфо: %s\n\n"
                                           % (g.date, g.place, g.time, g.max, g.info))
        my_num_in_this_game = amiingame(query.from_user['id'], ichoosegame)

        if my_num_in_this_game[0] >= 0:
            text_games = text_games + str("Ты в списке, молодец :)\n")
        num = check_players_amount(g)
        if (num == 0):
            text_games = text_games + str("Мест больше не осталось\n")

        else:
            one_game_keyboard = [[InlineKeyboardButton("Добавить друга", callback_data='addfriend:%s' % g.id)]]
            if (num > 0):
                text_games = text_games + str("Осталось мест: %s\n" % (num))
                if my_num_in_this_game[0] == -1: #если еще не записался
                    one_game_keyboard.insert(0, [InlineKeyboardButton("Играю %s" % (g.date), callback_data='wannaplay:%s' % g.id)])
            if (num < 0):
                text_games = text_games + str("Основной состав набран. Осталось мест в резерве: %s\n" % (-num))
                if my_num_in_this_game[0] == -1:  # если еще не записался
                    one_game_keyboard.insert(0, [InlineKeyboardButton("В резерв на %s\n" % (g.date), callback_data='wannaplay:%s' % g.id)])
        text_games = text_games + str("\nСостав участников:\n")
        for i in range(0, len(g.people)):
            if (i == int(g.max) ):
                text_games = text_games + str("== Резерв ==\n")
            if (i in my_num_in_this_game[1]):
                text_games = text_games + str("🌱️%s: *%s*\n" % (i+1, g.people[i][0].replace('<==','👈'))) #выделяем себя bold`ом #и своих друзей
            else: text_games = text_games + str("%s: %s\n" % (i+1, g.people[i][0].replace('<==','👈')))
        for i in my_num_in_this_game[1]:
            if i == my_num_in_this_game[0]:
                one_game_keyboard.append([InlineKeyboardButton("Удалить себя ❌", callback_data='can:%s:me' % ichoosegame)])
            else:
                one_game_keyboard.append([InlineKeyboardButton("Удалить %s ❌" %g.people[i][0].split(' <== ')[0],
                                                           callback_data='can:%s:%s' %(ichoosegame, g.people[i][0]))])
        one_game_keyboard.append([InlineKeyboardButton("⬅️ Назад", callback_data='gamelist')])

        reply_markup = telegram.InlineKeyboardMarkup(one_game_keyboard)
        mes = bot.edit_message_text(text=text_games, chat_id=query.message.chat_id, message_id=query.message.message_id,
                                    reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])


    if ('wannaplay' in query.data):
        if checktimeout(query.from_user['id'], 5, 2):
            bot.edit_message_text(text='Подожди еще пару секунд, мой нетерпеливый дружок...', chat_id=query.message.chat_id,
                                  message_id=query.message.message_id)
            get_games_info(bot)
            settimers(bot)
            ichoosegame = query.data
            ichoosegame = ichoosegame.split(':')[1]
            g = find_game(ichoosegame)
            print('I\'m joining user %s to game %s' % (us.name, g.date))
            ind = len(g.people) + 6
            am = check_players_amount(g)
            after_join_game_keyboard = []

            if (am > 0) or (am < 0):
                #append_range('%s!C%s:D%s' % (g.title, ind, ind), find_user(query.from_user['id']))
                join_game_db(ichoosegame, find_user(query.from_user['id']))
                g.people.append([find_user(query.from_user['id'])])
                send_msg_to_group(bot, g, us, 'add')
                my_num_in_this_game = amiingame(query.from_user['id'], ichoosegame)

                text_games = "😎 Ты в игре!\nЯ записал тебя на следующую игру:\n" + \
                             str("📅 Дата: %s\n🗺 Место: %s\n🕐 Начало: %s\nИнфо: %s\n\n" % (g.date, g.place, g.time, g.info))
                text_games = text_games + str("Состав участников:\n")
                for i in range(0, len(g.people)):
                    if (i == int(g.max)): text_games = text_games + str("== Резерв ==\n")
                    #if (i > 1) and (i in my_num_in_this_game[1]):
                    if i in my_num_in_this_game[1]:
                        text_games = text_games + str(
                            "🌱️%s: *%s*\n" % (i + 1, g.people[i][0].replace('<==','👈')))  # выделяем себя bold`ом #и своих друзей
                    else:
                        text_games = text_games + str("%s: %s\n" % (i + 1, g.people[i][0].replace('<==','👈')))
                if check_players_amount(g) != 0:
                    after_join_game_keyboard.append([InlineKeyboardButton("Добавить друга", callback_data='addfriend:%s' % ichoosegame)])
                for i in my_num_in_this_game[1]:
                    if i == my_num_in_this_game[0]:
                        after_join_game_keyboard.append(
                            [InlineKeyboardButton("Удалить себя ❌", callback_data='can:%s:me' % ichoosegame)])
                    else:
                        after_join_game_keyboard.append(
                            [InlineKeyboardButton("Удалить %s ❌" % g.people[i][0].split(' <== ')[0],
                                                  callback_data='can:%s:%s' % (ichoosegame, g.people[i][0]))])
                after_join_game_keyboard.append([InlineKeyboardButton("На главную", callback_data='gamelist')])
                reply_markup = telegram.InlineKeyboardMarkup(after_join_game_keyboard)
                mes = bot.edit_message_text(text=text_games, chat_id=query.message.chat_id, message_id=query.message.message_id,
                                      reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
                us.save_keyboard(mes['message_id'], mes['text'])
            else:
                #если места вдруг закончились
                text_game = "👿 Упс!\nМеста уже закончились."
                empty_markup = telegram.InlineKeyboardMarkup([[]])
                bot.edit_message_text(text=text_game, chat_id=query.message.chat_id, message_id=query.message.message_id,
                                      reply_markup=empty_markup)
                #telegram.ReplyKeyboardRemove()


    if ('addfriend' in query.data):
        ichoosegame = query.data.split(':')[1]
        g = find_game(ichoosegame)
        us.state = 11
        us.data = ichoosegame
        am = check_players_amount(find_game(ichoosegame))
        if am > 0:
            text_add = 'Ого, кажется, у нас намечается связка мечты.\nНапиши мне имена всех своих друзей через запятую.\n' \
                   'Осталось мест в основном составе: %s\nОстальные попадут в резерв.' %am
        elif am < 0:
            text_add = 'Ого, кажется, у нас намечается связка мечты.\nНапиши мне имена всех своих друзей через запятую.\n' \
                       'Основной состав набран.\nОсталось мест в резерве: %s\n' % (-am)
        add_friend_keyboard = [[InlineKeyboardButton("Назад", callback_data='showgame:%s' % g.id)]]
        reply_markup = telegram.InlineKeyboardMarkup(add_friend_keyboard)
        mes = bot.edit_message_text(text=text_add, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])

    if ('can' in query.data):
        ichoosegame = query.data.split(':')[1]
        canname = query.data.split(':')[2]
        cancel_confirm_keyboard = [[]]
        g = find_game(ichoosegame)
        if canname == 'me':
            text_cancel = 'Воу-воу, палехчи 🥺\nТы уверен, что хочешь выйти из игры %s?\nЕсли в резерве есть люди, они автоматически займут освободившееся место.' %g.date
            cancel_confirm_keyboard.append([InlineKeyboardButton("Да, покинуть игру", callback_data='cac:%s:%s' % (g.id, canname))])
        else:
            text_cancel = 'Воу-воу, палехчи 🥺\nТы уверен, что хочешь удалить из игры своего друга *%s*?\n' \
                          'Если в резерве есть люди, они автоматически займут освободившееся место.' % canname.split(' <== ')[0]
            cancel_confirm_keyboard.append(
                [InlineKeyboardButton("Да, он не придет", callback_data='cac:%s:%s' % (g.id, canname))])
        cancel_confirm_keyboard.append([InlineKeyboardButton("Назад", callback_data='showgame:%s' % g.id)])
        reply_markup = telegram.InlineKeyboardMarkup(cancel_confirm_keyboard)
        mes = bot.edit_message_text(text=text_cancel, chat_id=query.message.chat_id, message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])
        #print('I\'m canceling game number %s for user %s' % (g.id,canname))

    if ('cac' in query.data):
        ichoosegame = query.data.split(':')[1]
        canname = query.data.split(':')[2]
        g = find_game(ichoosegame)
        if canname == 'me':
            canceling_username = us.name
        else:
            canceling_username = canname
        for i in range(0, len(g.people)):
            if g.people[i][0] == canceling_username:
                g.people.pop(i)
                print('I\'m canceling game number %s for user %s' % (g.id, canceling_username))
                leave_game_db(g.id, canceling_username)
                if canname == 'me':
                    send_msg_to_group(bot, g, us, 'del')
                else:
                    send_msg_to_group(bot, g, us, 'del', frname=canname.split(' <== ')[0])
                notify_reserve(bot, i, g, canceling_username)
                text_cancel = 'Хорошо, я обновлю списки '
                cancel_finish_keyboard = [[InlineKeyboardButton("Назад", callback_data='showgame:%s' % g.id)],
                                          [InlineKeyboardButton("На главную", callback_data='gamelist')]]
                reply_markup = telegram.InlineKeyboardMarkup(cancel_finish_keyboard)
                mes = bot.edit_message_text(text=text_cancel, chat_id=query.message.chat_id,
                                      message_id=query.message.message_id,
                                      reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
                us.save_keyboard(mes['message_id'], mes['text'])

                return

    if ('confirmname' in query.data):
        new_name = query.data.split(':')[1]
        addusertodb(us.id, new_name)
        us.name = new_name
        us.state = 0
        spisokigr_keyboard = [['Список игр']]
        reply_markup = telegram.ReplyKeyboardMarkup(spisokigr_keyboard, resize_keyboard=True)
        bot.send_message(chat_id=query.message.chat_id,
                         text="Рад познакомиться, %s!\nА теперь нажми Список игр." % new_name,
                         reply_markup=reply_markup)

        empty_markup = telegram.InlineKeyboardMarkup([[]])

        bot.edit_message_text(chat_id=query.message.chat_id,
                              text="Ммм, %s... Как мелодично." % new_name,
                              message_id=query.message.message_id,
                              reply_markup=empty_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        return


    if ('adminpage' in query.data): #зеркало в команде
        us.state = 0
        if (str(query.from_user['id']) in admin_list):
            admin_keyboard = [[InlineKeyboardButton("Создать игру", callback_data='newgame')],
                              [InlineKeyboardButton("Изменить игру", callback_data='editsomegame')],
                              [InlineKeyboardButton("Удалить игру", callback_data='delsomegame')]]

            reply_markup = telegram.InlineKeyboardMarkup(admin_keyboard)
            mes = bot.edit_message_text(chat_id=query.message.chat_id,
                                  text="Привет, избранный!\nЧем займемся?",
                                  message_id=query.message.message_id,
                                  reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
            us.save_keyboard(mes['message_id'], mes['text'])


    if ('newgame' in query.data):
        if len(games.all) > 0:
            new_game_keyboard = [[InlineKeyboardButton("Ctrl+C", callback_data='copygame')],
                          [InlineKeyboardButton("Новая", callback_data='creategame')]]
        else:
            new_game_keyboard = [[InlineKeyboardButton("Новая", callback_data='creategame')]]

        reply_markup = telegram.InlineKeyboardMarkup(new_game_keyboard)
        mes = bot.edit_message_text(chat_id=query.message.chat_id,
                              text="Есть два варианта:\n"
                                   "1: Копипастнуть последнюю игру с переносом даты на неделю вперед\n"
                                   "2: Создать полностью новую игру, вручную введя ее детали",
                              message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])

    if ('copygame' in query.data):


        # парсим дату из предпоследней игры ,добавляем неделю, меняем тайтл нового листа
        lastdat = datetime.datetime(2019,1,1)
        for g in games.all:
            delta = g.dat - lastdat
            if delta.days > 0:
                lastdat = g.dat
        new_dat = lastdat + datetime.timedelta(days=7)
        new_date = get_date_with_zero(new_dat)

        games.all.append(copy.copy(games.all[-1]))
        g = games.all[-1]
        g.dat = new_dat
        g.date = new_date
        g.id = get_unique_id()
        g.people = []
        gametodb(g)

        print('I copied game. New game\'s date %s, id %s' % (g.date, g.id))
        send_msg_to_group(bot, g, us, 'newgame')
        notify_vip(bot, g)
        settimers(bot)
        copy_finish_keyboard = [[InlineKeyboardButton("В админку", callback_data='adminpage')],
                              [InlineKeyboardButton("К списку игр", callback_data='gamelist')]]

        reply_markup = telegram.InlineKeyboardMarkup(copy_finish_keyboard)
        mes = bot.edit_message_text(chat_id=query.message.chat_id,
                              text="Игра создана!\n\n📅 Дата: %s\n🗺 Место: %s\n🕐 Начало: %s\nИнфо: %s\nМест: %s" % (
                                 g.date, g.place, g.time, g.info, g.max),
                              message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])

    if ('creategame' in query.data):
        copy_finish_keyboard = [[InlineKeyboardButton("В админку", callback_data='adminpage')],
                                [InlineKeyboardButton("К списку игр", callback_data='gamelist')]]
        find_user_give_user(query.from_user['id']).state = 20
        reply_markup = telegram.InlineKeyboardMarkup(copy_finish_keyboard)
        mes = bot.edit_message_text(chat_id=query.message.chat_id,
                              text="Люблю все новое!\nОтправь мне детали новой игры, как в примере:\n"
                                   "_01/07, 12:00, Электрозаводская, 18 (игроков), Описание игры без запятых_",
                              message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])

    if ('delsomegame' in query.data):
        #выводим список игр
        del_games_keyboard = [[]]
        j = 0

        text_games = "Какую из игр хочешь удалить, *%s*?" % find_user(query.from_user['id'])
        if (len(games.all) > 0):
            for g in games.all:
                emo_num = '%s️⃣ ' % str(j + 1)
                text_games = text_games + str("\n\n%s%s, %s, %s,\n%s" % (emo_num, g.date, g.place, g.time, g.info))
                num = check_players_amount(g)
                del_games_keyboard.append([])
                del_games_keyboard[j].append(
                    InlineKeyboardButton("%sУдалить %s" % (emo_num, g.date), callback_data='delgame:%s' % g.id))
                if (num > 0): text_games = text_games + str("\nЗанято мест: %s из %s" % (str(int(g.max) - int(num)), g.max))
                else: text_games = text_games + str("\nЗанято мест: %s из %s" % (str(g.max), str(g.max)))
                j = j + 1
        del_games_keyboard.append([InlineKeyboardButton("В админку", callback_data='adminpage')])
        reply_markup = telegram.InlineKeyboardMarkup(del_games_keyboard)
        mes = bot.edit_message_text(chat_id=query.message.chat_id,
                              text=text_games,
                              message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])

    if ('delgame' in query.data):
        ichoosegame = query.data.split(':')[1]
        g = find_game(ichoosegame)
        text_game = 'Ты уверен, что хочешь *удалить* эту игру?\n\n' + str("📅 Дата: %s\n🗺 Место: %s\n🕐 Начало: %s\n"
                                                                          "Мест всего: %s\nИнфо: %s\n\n"
                                           % (g.date, g.place, g.time, g.max, g.info))
        del_keyboard = [[InlineKeyboardButton("Да", callback_data='del_gameconfirm:%s' % ichoosegame)],
                        [InlineKeyboardButton("Назад", callback_data='delsomegame')],
                        [InlineKeyboardButton("В админку", callback_data='adminpage')]]
        reply_markup = telegram.InlineKeyboardMarkup(del_keyboard)
        print('I wanna delete game ID %s' % ichoosegame)
        mes = bot.edit_message_text(chat_id=query.message.chat_id,
                              text=text_game,
                              message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])

    if ('del_gameconfirm' in query.data):
        ichoosegame = query.data.split(':')[1]

        send_msg_to_group(bot, find_game(ichoosegame), us, 'delgame')
        for i in range(len(games.all)):
            if str(games.all[i].id) == str(ichoosegame):
                games.all.pop(i)
                break
        delgamefromdb(ichoosegame)
        print('Game deleted %s' % ichoosegame)
        del_keyboard = [[InlineKeyboardButton("В админку", callback_data='adminpage')]]
        reply_markup = telegram.InlineKeyboardMarkup(del_keyboard)
        mes = bot.edit_message_text(chat_id=query.message.chat_id,
                              text='Игра удалена',
                              message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])

    if ('editsomegame' in query.data):
        #выводим список игр
        edit_games_keyboard = [[]]
        j = 0

        text_games = "Какую из игр будем редактировать, *%s*?" % find_user(query.from_user['id'])
        if (len(games.all) > 0):
            for g in games.all:
                emo_num = '%s️⃣ ' % str(j + 1)
                text_games = text_games + str("\n\n%s%s, %s, %s,\n%s" % (emo_num, g.date, g.place, g.time, g.info))
                num = check_players_amount(g)
                edit_games_keyboard.append([])
                edit_games_keyboard[j].append(
                    InlineKeyboardButton("%sИзменить %s" % (emo_num, g.date), callback_data='editgame:%s' % g.id))
                if (num > 0): text_games = text_games + str("\nЗанято мест: %s из %s" % (str(num), str(g.max)))
                else: text_games = text_games + str("\nЗанято мест: %s из %s" % (str(g.max), str(g.max)))
                j = j + 1
        edit_games_keyboard.append([InlineKeyboardButton("В админку", callback_data='adminpage')])
        reply_markup = telegram.InlineKeyboardMarkup(edit_games_keyboard)
        mes = bot.edit_message_text(chat_id=query.message.chat_id,
                              text=text_games,
                              message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])

    if ('editgame' in query.data):
        ichoosegame = query.data.split(':')[1]
        g = find_game(ichoosegame)
        text_game = 'Какой параметр будем редактировать?\n\n' + str("📅 Дата: %s\n🗺 Место: %s\n🕐 Начало: %s\n"
                                                                          "Мест всего: %s\nИнфо: %s\n\n"
                                           % (g.date, g.place, g.time, g.max, g.info))
        for i in range(0, len(g.people)):
            if (i == int(g.max)): text_game = text_game + str("== Резерв ==\n")
            text_game = text_game + str("%s: %s\n" % (i + 1, g.people[i][0]))
        edit_keyboard = [[InlineKeyboardButton("Дата", callback_data='editdata:date:%s' % ichoosegame)],
                        [InlineKeyboardButton("Время", callback_data='editdata:time:%s' % ichoosegame)],
                        [InlineKeyboardButton("Место", callback_data='editdata:place:%s' % ichoosegame)],
                        [InlineKeyboardButton("Количество участников", callback_data='editdata:max:%s' % ichoosegame)],
                        [InlineKeyboardButton("Инфо", callback_data='editdata:info:%s' % ichoosegame)],
                        [InlineKeyboardButton("Удалить игроков", callback_data='dlp:%s' % ichoosegame)],
                        [InlineKeyboardButton("Удалить всех", callback_data='delallplayers:%s' % ichoosegame)],
                        [InlineKeyboardButton("В админку", callback_data='adminpage')]]
        reply_markup = telegram.InlineKeyboardMarkup(edit_keyboard)
        mes = bot.edit_message_text(chat_id=query.message.chat_id,
                              text=text_game,
                              message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])

    if ('editdata' in query.data):
        ichoosegame = query.data.split(':')[2]
        what = str(query.data.split(':')[1])
        g = find_game(ichoosegame)
        us = find_user_give_user(query.from_user['id'])
        if what == 'date':
            us.state = 21
            text_game = 'Окей, введи новую дату в виде: 23/01. Если нужно, укажи дополнительно год: 23/01/20'
        if what == 'time':
            us.state = 22
            text_game = 'Окей, введи новое время в виде: 13:00'
        if what == 'place':
            us.state = 23
            text_game = 'Окей, отправь мне новое место игры'
        if what == 'max':
            us.state = 24
            text_game = 'Окей, введи новое максимальное количество людей на игру. Только число.'
        if what == 'info':
            us.state = 25
            text_game = 'Окей, введи новую инфу об игре'
        us.data = ichoosegame
        edit_keyboard = [[]]
        reply_markup = telegram.InlineKeyboardMarkup(edit_keyboard)
        bot.edit_message_text(chat_id=query.message.chat_id,
                              text=text_game,
                              message_id=query.message.message_id,
                              reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

    if ('delallplayers' in query.data):
        ichoosegame = query.data.split(':')[1]
        g = find_game(ichoosegame)
        for i in range(len(g.people), 0):
            g.people.pop(i)
        update_game_data(g.id, cleanusers=True)
        all_del_keyboard = [[InlineKeyboardButton(text='В админку', callback_data='adminpage')]]

        reply_markup = telegram.InlineKeyboardMarkup(all_del_keyboard)
        mes = bot.edit_message_text(chat_id=query.message.chat_id,
                              text='Ты всех удалил. Не стыдно?',
                              message_id=query.message.message_id,
                              reply_markup=reply_markup,
                              parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])

    if ('dlp' in query.data):
        ichoosegame = query.data.split(':')[1]
        g = find_game(ichoosegame)
        k = 0
        text_game = 'Это админская панель для быстрого удаления игроков.\nОсторожней, люди удаляются *без подтверждения*.\n' + \
                    str("📅 Дата: %s\n🗺 Место: %s\n🕐 Начало: %s\n"
                        "Мест всего: %s\nИнфо: %s\n\n"
                        % (g.date, g.place, g.time, g.max, g.info))

        if len(query.data.split(':')) == 2: #если пришли в этот блок первый раз
            mass_del_keyboard = [[]]
            del_list.people = []

            for i in range(0, len(g.people)):
                del_list.people.append([i, g.people[i][0]])

        if len(query.data.split(':')) > 2:                 #если этот блок вызвал сам себя, то находим чувака для удаления
            canceling_username = query.data.split(':')[2]
            for i in range(0, len(g.people)):
                if str(g.people[i][0]) == str(canceling_username):
                    for j in range(len(del_list.people)):
                        if del_list.people[j][1] == g.people[i][0]:
                            del_list.people.pop(j)
                            break

                    g.people.pop(i)
                    leave_game_db(g.id, canceling_username)
                    print('I\'ve deleted %s from game %s' % (canceling_username, g.id))
                    notify_reserve(bot, i, g, canceling_username)
                    send_msg_to_group(bot, g, us, 'del', frname=canceling_username.split(' <== ')[0])

                    break

        mass_del_keyboard = [[]]
        for i in range(0, len(del_list.people)):
            if (i == int(g.max)): text_game = text_game + str("== Резерв ==\n")
            text_game = text_game + str("%s: %s\n" % (str(int(del_list.people[i][0]) + 1), del_list.people[i][1]))

            if i // 4 > k:
                k = k + 1
                mass_del_keyboard.append([])
            mass_del_keyboard[k].append(InlineKeyboardButton(text='%s' % (str(int(del_list.people[i][0]) + 1)), callback_data='dlp:%s:%s'
                                                                % (str(g.id), str(del_list.people[i][1]))))
        mass_del_keyboard.append([InlineKeyboardButton(text='В админку', callback_data='adminpage')])

        reply_markup = telegram.InlineKeyboardMarkup(mass_del_keyboard)
        mes = bot.edit_message_text(chat_id=query.message.chat_id,
                              text=text_game,
                              message_id=query.message.message_id,
                              reply_markup=reply_markup,
                              parse_mode=telegram.ParseMode.MARKDOWN)
        us.save_keyboard(mes['message_id'], mes['text'])




def echo(bot, update):
    print(update.message.text)
    if str(update.message.chat_id) != str(groupid):
        if login(update.message.from_user.id, update.message.chat_id, bot):
            us = find_user_give_user(update.message.from_user.id)
            if (update.message.text == "300"):
                bot.send_message(chat_id=update.message.chat_id, text="Ты сам знаешь, что делать")

                conn = sqlite3.connect("mydatabase.db")
                cursor = conn.cursor()
                sql = "SELECT date FROM games"
                cursor.execute(sql)
                conn.commit()
                for a in cursor.fetchall():
                    if len(a[0].split('/')) < 3:
                        cursor.execute("""UPDATE games SET date = (?) WHERE date = (?)""", (str(a[0]) + '/2019', a[0]) )
                        conn.commit()


            elif (update.message.text == "Список игр"):

                bot.send_message(chat_id=update.message.chat_id, text='Думаю... Пару секунд... Как у тебя дела вообще? Что нового?\n'
                                                                      '_отвечать не надо, я робот, мне всё равно_ 🤖',
                                 parse_mode=telegram.ParseMode.MARKDOWN)
                us.state = 0
                if checktimeout(update.message.from_user.id, 5, 1):
                    get_games_info(bot)
                    settimers(bot)

                    games_keyboard = [[]]
                    j = 0

                    reply_markup = telegram.InlineKeyboardMarkup(games_keyboard)

                    text_games = "Смотри, какие игры скоро состоятся:\nКуда запишемся, *%s*?" %find_user(update.message.from_user.id)
                    print('LEN GAMES %s' % len(games.all))
                    if (len(games.all) > 0):

                        for g in games.all:
                            emo_num = '%s️⃣ ' % str(j+1)
                            text_games = text_games + str("\n\n%s%s, %s, %s,\n%s" % (emo_num, g.date, g.place, g.time, g.info))

                            num = check_players_amount(g)
                            games_keyboard.append([])
                            games_keyboard[j].append(
                                InlineKeyboardButton("%sИгра %s" % (emo_num, g.date), callback_data='showgame:%s' % g.id))
                            if (num == 0): text_games = text_games + str("\nМест в основном составе и резерве больше нет")
                            if (num > 0): text_games = text_games + str("\nОсталось мест: %s" % (num))
                            if (num < 0): text_games = text_games + str("\nОсновной состав набран. Осталось мест в резерве: %s" % (-num))
                            j = j + 1
                        us.destroy_keyboard(bot, update)
                        mes = bot.send_message(chat_id=update.message.chat_id, text=text_games, reply_markup=reply_markup,
                                         parse_mode=telegram.ParseMode.MARKDOWN)
                        us.save_keyboard(mes['message_id'], mes['text'])
                    else: bot.send_message(chat_id=update.message.chat_id, text="Я не нашел ни одной игры")


            elif (us.state == 11): #имена друзей
                ichoosegame = us.data
                names = update.message.text.split(',')
                if len(names) > 10:
                    after_join_friend_keyboard = [[InlineKeyboardButton("Назад", callback_data='showgame:%s' %ichoosegame)]]
                    reply_markup = telegram.InlineKeyboardMarkup(after_join_friend_keyboard)
                    us.destroy_keyboard(bot, update)
                    mes = bot.send_message(chat_id=update.message.chat_id, text='Перебор!\nНе больше 10 человек.',
                                     reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
                    us.save_keyboard(mes['message_id'], mes['text'])
                    return
                for i in names:
                    # if len(i) > 12:
                    test_str = 'aaa:999999:%s <== %s' % (i, find_user_give_user(update.message.from_user.id).name)
                    print('Testing\n', test_str, '\nSize is', sys.getsizeof(test_str))
                    if sys.getsizeof(test_str) > 138:
                        bot.send_message(chat_id=update.message.chat_id, text='Ой, одно из имен слишком длинное, мне не нравится.\n'
                                                                              'Попробуй сократить его.')
                        return
                    if ('*' in i) or ('_' in i) or ('\'' in i):
                        bot.send_message(chat_id=update.message.chat_id,
                                         text='Я обнаружил запрещенные символы.\n'
                                              'Мне будет от них плохо, поправь.')
                        return
                get_games_info(bot)
                settimers(bot)
                g = find_game(ichoosegame)
                am = check_players_amount(g)
                ichoosegame = us.data
                ind = len(g.people) + 6
                if ((am > 0) and (len(names) <= am + int(g.res))) or ((am < 0) and (len(names) <= (-am))):
                    for i in range(len(names)):
                        names[i] = names[i].strip(' ')
                        if names[i] == '': continue
                        #append_range('%s!C%s' % (g.title, ind + i), '%s <== %s' % (names[i], us.name))
                        join_game_db(ichoosegame, '%s <== %s' % (names[i], us.name))
                        g.people.append(['%s <== %s' % (names[i], us.name)])
                        send_msg_to_group(bot, g, us, 'add', frname=names[i])
                    my_num_in_this_game = amiingame(update.message.from_user.id, ichoosegame)
                    text_games = "😎 Получилось!\nЗаписал на следующую игру:\n" + \
                                 str("📅 Дата: %s\n🗺 Место: %s\n🕐 Начало: %s\nИнфо: %s\n\n" % (
                                     g.date, g.place, g.time, g.info))
                    text_games = text_games + str("Состав участников:\n")
                    for i in range(len(g.people)):
                        if (i == int(g.max)): text_games = text_games + str("== Резерв ==\n")
                        if (i in my_num_in_this_game[1]):
                            text_games = text_games + str("🌱️%s: *%s*\n" % (i + 1, g.people[i][0].replace('<==','👈')))  # выделяем себя bold`ом #и своих друзей
                        else:
                            text_games = text_games + str("%s: %s\n" % (i + 1, g.people[i][0].replace('<==','👈')))
                    after_join_friend_keyboard = []
                    for i in my_num_in_this_game[1]:
                        if i == my_num_in_this_game[0]:
                            after_join_friend_keyboard.append(
                                [InlineKeyboardButton("Удалить себя ❌", callback_data='can:%s:me' % ichoosegame)])
                        else:
                            after_join_friend_keyboard.append(
                                [InlineKeyboardButton("Удалить %s ❌" % g.people[i][0].split(' <== ')[0],
                                                      callback_data='can:%s:%s' % (ichoosegame, g.people[i][0]))])
                    after_join_friend_keyboard.append([InlineKeyboardButton("На главную", callback_data='gamelist')])
                    reply_markup = telegram.InlineKeyboardMarkup(after_join_friend_keyboard)
                    us.destroy_keyboard(bot, update)
                    mes = bot.send_message(chat_id=update.message.chat_id, text=text_games, reply_markup=reply_markup,
                                     parse_mode=telegram.ParseMode.MARKDOWN)
                    us.save_keyboard(mes['message_id'], mes['text'])
                else:
                    # если места вдруг закончились
                    text_games = "👿 Упс!\nМеста уже закончились."
                    empty_markup = telegram.InlineKeyboardMarkup([[]])

                    bot.send_message(chat_id=update.message.chat_id, text=text_games,
                                     parse_mode=telegram.ParseMode.MARKDOWN)
                us.state = 0

            elif (us.state == 20):
                #новая игра
                #паттерн: 01/07, 12:00, Электрозаводская, 18, Описание игры
                rawdata = update.message.text.split(',')

                if len(rawdata) != 5:
                    bot.send_message(chat_id=update.message.chat_id, text='Ошибка. Я ожидал получить 4 запятых.',
                                     parse_mode=telegram.ParseMode.MARKDOWN)
                    return
                if not ('/' in rawdata[0]):
                    bot.send_message(chat_id=update.message.chat_id, text='Ошибка. В дате игры отсутствует необходимый разделитель.\n'
                                                                          'Формат даты: 09/11',
                                     parse_mode=telegram.ParseMode.MARKDOWN)
                    return
                if len(rawdata[0].split('/')) > 2:
                    if (int(rawdata[0].split('/')[2]) < 1000):
                        newdat = datetime.datetime(int(int(rawdata[0].split('/')[2]) + 2000), int(rawdata[0].split('/')[1]),
                                                   int(rawdata[0].split('/')[0]))
                    else:
                        newdat = datetime.datetime(int(rawdata[0].split('/')[2]), int(rawdata[0].split('/')[1]),
                                                   int(rawdata[0].split('/')[0]))
                else:
                    newdat = datetime.datetime(datetime.datetime.now().year, int(rawdata[0].split('/')[1]), int(rawdata[0].split('/')[0]))
                newdate = get_date_with_zero(newdat)


                if not (':' in rawdata[1]):
                    bot.send_message(chat_id=update.message.chat_id, text='Ошибка. Во времени игры отсутствует необходимый разделитель.\n'
                                                                          'Формат времени: 17:30',
                                     parse_mode=telegram.ParseMode.MARKDOWN)
                    return
                newtime = rawdata[1].strip(' ')
                newplace = rawdata[2].strip(' ')
                try:
                    newmax = int(rawdata[3])
                except ValueError:
                    bot.send_message(chat_id=update.message.chat_id,
                                     text='Ошибка. На предпоследнем (4м) месте должно стоять количество игроков, только число.',
                                     parse_mode=telegram.ParseMode.MARKDOWN)
                    return
                newinfo = rawdata[4].strip(' ')

                if len(games.all): games.all.append(copy.copy(games.all[-1]))
                else:
                    games.all = []
                    games.all.append(Game(1234567890, newdate, 1000001))
                g = games.all[-1]
                g.people = []
                g.dat = newdat
                g.date = newdate
                g.time = newtime
                g.place = newplace
                g.info = newinfo
                g.max = newmax
                g.id = get_unique_id()
                gametodb(g)
                send_msg_to_group(bot, g, us, 'newgame')
                notify_vip(bot, g)

                copy_finish_keyboard = [[InlineKeyboardButton("В админку", callback_data='adminpage')],
                                        [InlineKeyboardButton("К списку игр", callback_data='gamelist')]]
                reply_markup = telegram.InlineKeyboardMarkup(copy_finish_keyboard)
                us.destroy_keyboard(bot, update)
                mes = bot.send_message(text="Игра создана!\n\n📅 Дата: %s\n🗺 Место: %s\n🕐 Начало: %s\nИнфо: %s\nМест: %s" % (
                                          g.date, g.place, g.time, g.info, g.max),
                                 chat_id=update.message.chat_id,
                                 message_id=update.message.message_id,
                                 reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
                us.save_keyboard(mes['message_id'], mes['text'])
                us.state = 0


            elif (us.state == 21): #дата
                g = find_game(us.data)
                if not ('/' in update.message.text):
                    bot.send_message(chat_id=update.message.chat_id, text='Ошибка. В дате игры отсутствует необходимый разделитель.\n'
                                                                          'Формат даты: 09/11',
                                     parse_mode=telegram.ParseMode.MARKDOWN)
                    return
                rawdata = update.message.text.split('/')
                newdat = datetime.datetime(datetime.datetime.now().year, int(rawdata[1]), int(rawdata[0]))
                g.dat = newdat
                g.date = update.message.text
                g.title = g.date
                update_game_data(g.id, ifdate=True, date=g.date)
                settimers(bot)
            elif (us.state == 22):  #время
                g = find_game(us.data)
                rawdata = update.message.text
                if not (':' in rawdata):
                    bot.send_message(chat_id=update.message.chat_id, text='Ошибка. Во времени игры отсутствует необходимый разделитель.\n'
                                                                          'Формат времени: 17:30',
                                     parse_mode=telegram.ParseMode.MARKDOWN)
                    return
                g.time = rawdata
                update_game_data(g.id, iftime=True, time=rawdata)
            elif (us.state == 23):  #место
                g = find_game(us.data)
                rawdata = update.message.text
                g.place = rawdata
                update_game_data(g.id, ifplace=True, place=rawdata)
            elif (us.state == 24):  #люди
                g = find_game(us.data)
                rawdata = update.message.text
                try:
                    newmax = int(rawdata)
                except ValueError:
                    bot.send_message(chat_id=update.message.chat_id, text='Ошибка. Я ждал число.', parse_mode=telegram.ParseMode.MARKDOWN)
                    return
                g.max = newmax
                update_game_data(g.id, ifmax=True, nmax=newmax)
            elif (us.state == 25):  #инфо
                g = find_game(us.data)
                rawdata = update.message.text
                g.info = rawdata
                update_game_data(g.id, ifinfo=True, info=rawdata)

            else:
                pass
                #bot.send_message(chat_id=update.message.chat_id, text="Бесплатно эту команду выполнять я отказываюсь.")

            if (us.state == 21) or (us.state == 22) or (us.state == 23) or (us.state == 24) or (us.state == 25):
                g = find_game(us.data)
                edit_finish_keyboard = [[InlineKeyboardButton("Изменить что-то еще", callback_data='editgame:%s' % g.id)],
                                        [InlineKeyboardButton("В админку", callback_data='adminpage')],
                                        [InlineKeyboardButton("К списку игр", callback_data='gamelist')]]
                reply_markup = telegram.InlineKeyboardMarkup(edit_finish_keyboard)
                us.destroy_keyboard(bot, update)
                mes = bot.send_message(text="Игра отредактирована!\n\n📅 Дата: %s\n🗺 Место: %s\n🕐 Начало: %s\nИнфо: %s\nМест: %s" % (
                    g.date, g.place, g.time, g.info, g.max),
                                 chat_id=update.message.chat_id,
                                 message_id=update.message.message_id,
                                 reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
                us.save_keyboard(mes['message_id'], mes['text'])
                us.state = 0



        elif find_user_give_user(update.message.from_user.id) is not None: #если был не залогинен, но болванку под юзера уже создали
            us = find_user_give_user(update.message.from_user.id)
            new_name = update.message.text.strip(' ')
            if (us.state == 1) and (new_name != 'Список игр'):  # вводим имя
                # проверяем имя на уникальность
                if len(new_name) > 17:
                    bot.send_message(chat_id=update.message.chat_id,
                                     text="Очень длинное имя\nДавай покороче, а?")
                    return
                for u in users.all:
                    if str(u.name) == str(new_name):
                        bot.send_message(chat_id=update.message.chat_id,
                                         text="Это имя уже занято\nПопробуй добавить первую букву фамилии, например \"Владимир П.\" ")
                        return
                confirm_name_keyboard = [[InlineKeyboardButton("Да", callback_data='confirmname:%s' % new_name)]]
                reply_markup = telegram.InlineKeyboardMarkup(confirm_name_keyboard)
                mes = bot.send_message(text="*%s* - это твое имя?\nМне запомнить тебя так?\nЕсли все ок, просто нажми Да.\n"
                                      "Если тебе не нравится это имя, просто введи новое" % new_name,
                                 chat_id=update.message.chat_id,
                                 message_id=update.message.message_id,
                                 reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)
                us.save_keyboard(mes['message_id'], mes['text'])




def req_range(range):
    value_render_option = 'FORMATTED_VALUE'  #
    date_time_render_option = 'SERIAL_NUMBER'  #
    request = service.spreadsheets().values().batchGet(spreadsheetId=spr_id, ranges=range,
                                                       valueRenderOption=value_render_option,
                                                       dateTimeRenderOption=date_time_render_option)
    response = request.execute()
    return response.get("valueRanges")

def append_range(range, name, name2=None):
    #includeValuesInResponse = true  #
    value_input_option = 'RAW'
    responseDateTimeRenderOption = 'FORMATTED_STRINGS'  #
    responseValueRenderOption = 'FORMATTED_VALUE'
    insert_data_option = 'OVERWRITE'
    value_range_body = {"values": [[str(name)]]}
    if not (name2 is None): value_range_body = {"values": [[str(name)]]}
    request = service.spreadsheets().values().append(spreadsheetId=spr_id, range=range, valueInputOption=value_input_option,
                                                     insertDataOption=insert_data_option, body=value_range_body)
    response = request.execute()

def notify_vip(bot, g):
    text = '📡🏐⚡️ Мои радары засекли новую игру!\nОна начнется %s в %s\nЛокация: %s\nДетали: %s' \
                  % (g.date, g.time, g.place, g.info)
    for uid in vip_list:
        one_game_keyboard = [[InlineKeyboardButton("Играю %s" % (g.date), callback_data='wannaplay:%s' % g.id)]]
        reply_markup = telegram.InlineKeyboardMarkup(one_game_keyboard)
        bot.send_message(text=text, chat_id=uid, reply_markup=reply_markup, parse_mode=telegram.ParseMode.MARKDOWN)

def send_msg_to_group(bot, g, us, action, frname=''):
    #return 0
    texttogroup = ''
    am = check_players_amount(g)
    if action == 'add':
        if frname == '': #нет имени друга - записываем себя
            if (am > 0) or (am == -int(g.res)):
                i = random.randint(1, 8)
                if i == 1: texttogroup = '*%s* играет *%s*. Правда, здорово?' % (us.name, g.date)
                if i == 2: texttogroup = '*%s* записывается на тренировку *%s*. %s молодец, будь, как %s.' % (us.name, g.date, us.name, us.name)
                if i == 3: texttogroup = 'Представляете, *%s* будет не валяться на диване *%s*, а заниматься спортом. Я в восторге.' % (us.name, g.date)
                if i == 4: texttogroup = '*%s* изучает новый трюк и покажет его всем *%s*.' % (us.name, g.date)
                if i == 5: texttogroup = '*%s* раздает автографы *%s*, не пропустите.' % (us.name, g.date)
                if i == 6: texttogroup = '*%s* хочет похвастаться новыми леггинсами на тренировке *%s*.' % (us.name, g.date)
                if i == 7: texttogroup = '*%s* ставит ящик пива победившей команде *%s*.' % (us.name, g.date)
                if i == 8: texttogroup = 'Ребят, я не смогу прийти на игру *%s*, но вместо меня обещал поиграть *%s*.' % (g.date, us.name)
            elif (am < 0) or (am == 0):
                i = random.randint(1, 4)
                if i == 1: texttogroup = '*%s* записывается в резерв на игру *%s*. Люблю оптимистов.' % (us.name, g.date)
                if i == 2: texttogroup = '*%s* считает, что поздно - лучше, чем никогда, поэтому попадает в резерв на *%s*.' % (us.name, g.date)
                if i == 3: texttogroup = '*%s* просыпается в последний момент и размашисто вписывает себя в резерв на *%s*.' % (us.name, g.date)
                if i == 4: texttogroup = '*%s* надеется, что кто-то из основного состава загуляет накануне игры *%s*, чтоб чисто по-дружески его ' \
                                         'подменить из резерва.' % (us.name, g.date)

        else:
            i = random.randint(1, 8)
            if i == 1: texttogroup = '*%s* записывает *%s* на игру *%s*. Правда, здорово?' % (us.name, frname, g.date)
            if i == 2: texttogroup = '*%s* проявляет дружелюбие и вносит в список на игру *%s* *%s*.' % (us.name, g.date, frname)
            if i == 3: texttogroup = '*%s* ушел в геологическую экспедицию без связи и попросил *%s* записать его на игру *%s*' % (frname, us.name, g.date)
            if i == 4: texttogroup = '*%s* заблокирован Роскомнадзором, но на помощь приходит *%s* и записывает на игру *%s*' % (frname, us.name, g.date)
            if i == 5: texttogroup = '*%s* считает, что технологии зло, но волейбол любит, поэтому *%s* вносит его в списки на *%s*' % (frname, us.name, g.date)
            if i == 6: texttogroup = '*%s* и *%s* поспорили, чей шпагат глубже *%s*' % (frname, us.name, g.date)
            if i == 7: texttogroup = 'У *%s* закончилась платная подписка на бота, поэтому *%s* приходится записать его на *%s*' % (frname, us.name, g.date)
            if i == 8: texttogroup = '*%s* считает, что *%s* пора похудеть, поэтому записывает его на *%s*' % (us.name, frname, g.date)

        if am == -int(g.res):
            texttogroup = texttogroup + '\nКстати, это было последнее место. Основной состав набран, ура!'
        else:
            return #уведомление только если чел был последним

    if (action == 'del') and (am > 0):
        if frname == '':
            i = random.randint(1, 7)
            if i == 1: texttogroup = 'Волейболист *%s* покинул игру *%s*. Как же так?' % (us.name, g.date)
            if i == 2: texttogroup = '*%s* случайно разливает кофе на смартфон и удаляется из игры *%s*. Сочувствуем.' % (us.name, g.date)
            if i == 3: texttogroup = '*%s* убегает шопиться в торгуху, поэтому не придет *%s*. Присоединится, когда закончится скидки.' % (us.name, g.date)
            if i == 4: texttogroup = '*%s* узнает о существовании дочери. Дочь отчаянно намекала об этом последние 5 лет. Поэтому *%s* не ждите.' % (us.name, g.date)
            if i == 5: texttogroup = '*%s* решил попасовать *%s*. Но не под сеткой, а где-то еще. Нда.' % (us.name, g.date)
            if i == 6: texttogroup = '*%s* остается *%s* дома работать, т.к. в рабочее время нужно было работать, а не тусить в телеге и ржать. Похлопаем за такую ответственность!.' % (us.name, g.date)
            if i == 7: texttogroup = 'У *%s* операция по установке кибернетических икр и пневматического голеностопа *%s*. В этот раз не придёт, зато в следующий ой берегитесь' % (us.name, g.date)
        else:
            i = random.randint(1, 3)
            if i == 1: texttogroup = '*%s* говорит, что *%s* не придет на игру *%s*. Вот это поворот.' % (us.name, frname, g.date)
            if i == 2: texttogroup = 'У *%s* дома кошка тыкнула лапкой в телефон и удалила *%s* из игры *%s*. Мы не обижаемся на кошку, потому что у нее лапки.' % (us.name, frname, g.date)
            if i == 3: texttogroup = '*%s* завидует технике игры у *%s* и удаляет *%s*. Завидовать плохо.' % (us.name, frname, g.date)
        texttogroup = texttogroup + '\nОсталось *%s* свободных мест' % am

    if action == 'newgame':
        texttogroup = '📡🏐⚡️ Мои радары засекли новую игру!\nОна начнется %s в %s\nЛокация: %s\nДетали: %s' \
                      % (g.date, g.time, g.place, g.info)

    if action == 'delgame':
        texttogroup = '🤷‍♂️ Событие удалено.\n%s, %s\nЛокация: %s\nДетали: %s' \
                      % (g.date, g.time, g.place, g.info)

    if texttogroup != '':
        bot.send_message(text=texttogroup, chat_id=groupid, parse_mode=telegram.ParseMode.MARKDOWN)



get_all_users()
#get_games_info(bot)

del_list = Game(123,'123',123)

if __name__ == '__main__':



    dispatcher = updater.dispatcher
    logging.basicConfig(format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                        level=logging.INFO)
    start_handler = CommandHandler("start", start, pass_args=True)
    join_handler = CommandHandler("join", join, pass_args=True)
    trista_handler1 = CommandHandler("300", trista1)
    admin_handler = CommandHandler("adm", admin)
    invite_handler = CommandHandler("invite", invite)
    insert_handler = CommandHandler("insert", insert, pass_args=True)
    showusers_handler = CommandHandler("showusers", showusers)
    echo_handler = MessageHandler(Filters.text, echo)
    in_handler = CallbackQueryHandler(react_inline)
    dispatcher.add_handler(start_handler)
    dispatcher.add_handler(echo_handler)
    dispatcher.add_handler(in_handler)
    dispatcher.add_handler(admin_handler)
    dispatcher.add_handler(trista_handler1)
    dispatcher.add_handler(join_handler)
    dispatcher.add_handler(invite_handler)
    dispatcher.add_handler(insert_handler)
    dispatcher.add_handler(showusers_handler)
    dispatcher.add_handler(CommandHandler("kick", kick, pass_args=True))

    updater.start_polling()

    print('start')


